<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Fazaa Abdillah, das213, asda asda, A_3e76be</name>
   <tag></tag>
   <elementGuidId>3a7e0619-cb01-42aa-8485-8dc29bb2eb53</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#billing-address-select</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='billing-address-select']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>48b19955-8cd7-4d81-aade-fbb466e5a623</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>billing_address_id</value>
      <webElementGuid>85457bac-3dcc-4ac9-8610-2ae14d871e9d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>billing-address-select</value>
      <webElementGuid>4ae13a60-7017-4711-9397-f499512781cb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>address-select</value>
      <webElementGuid>f60f5fe6-d1b9-48e1-8256-c5f6c61421e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>Billing.newAddress(!this.value)</value>
      <webElementGuid>c886f9c9-1612-4bf4-a7eb-65445a92bf69</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        Fazaa Abdillah, das213, asda asda, Algeria
                        Fazaa Abdillah, asdas, asd 1223, Indonesia
                    New Address
                </value>
      <webElementGuid>0e2c9500-1c15-4348-91d1-c26dea5d6f77</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;billing-address-select&quot;)</value>
      <webElementGuid>a847f544-3ac1-43bf-b353-3787bb72c977</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='billing-address-select']</value>
      <webElementGuid>6fe2ad83-4ea6-4a9c-9865-817b5bb8db58</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-billing-load']/div/div/div/select</value>
      <webElementGuid>644f2a5b-f277-4a7b-8d84-bd16b47d97b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select a billing address from your address book or enter a new address.'])[1]/following::select[1]</value>
      <webElementGuid>63f8604e-f998-49a1-aca7-e683e347b68c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Billing address'])[1]/following::select[1]</value>
      <webElementGuid>64ce63c1-e9a3-4778-b5bb-7e4022dbb689</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='First name:'])[1]/preceding::select[1]</value>
      <webElementGuid>2aa925da-dae6-46c0-8d14-2f025cec5f0d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[1]/preceding::select[1]</value>
      <webElementGuid>8fd5a190-394d-4a2e-a1a2-38185a6b201e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>bfa17e3e-18e8-4692-8379-2d06a880b5a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'billing_address_id' and @id = 'billing-address-select' and (text() = '
                        Fazaa Abdillah, das213, asda asda, Algeria
                        Fazaa Abdillah, asdas, asd 1223, Indonesia
                    New Address
                ' or . = '
                        Fazaa Abdillah, das213, asda asda, Algeria
                        Fazaa Abdillah, asdas, asd 1223, Indonesia
                    New Address
                ')]</value>
      <webElementGuid>2cbeb41c-0f94-40bd-a2c7-32adf6d056c5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

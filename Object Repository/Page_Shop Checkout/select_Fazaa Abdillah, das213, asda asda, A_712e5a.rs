<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Fazaa Abdillah, das213, asda asda, A_712e5a</name>
   <tag></tag>
   <elementGuidId>105458b1-5844-4b11-95d9-fa23f6c38c9a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#shipping-address-select</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='shipping-address-select']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>a8dfac4b-95ad-49e4-ae20-76e2ca24f621</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>shipping_address_id</value>
      <webElementGuid>20f2e768-c2e3-4ab3-947d-0f32fa98a84d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>shipping-address-select</value>
      <webElementGuid>4ff86ba6-fb75-468c-9c49-87ff7ccc5425</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>address-select</value>
      <webElementGuid>581e8703-a9ae-405c-a789-c55a39f30a26</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>Shipping.newAddress(!this.value)</value>
      <webElementGuid>db807f37-b2a4-45eb-b8ec-29516af3fe3e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            Fazaa Abdillah, das213, asda asda, Algeria
                            Fazaa Abdillah, asdas, asd 1223, Indonesia
                        New Address
                    </value>
      <webElementGuid>7e07600b-1757-4ba1-9402-1789feb98ca1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;shipping-address-select&quot;)</value>
      <webElementGuid>4694abdb-4898-4c6e-9d04-46d721b3b31a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='shipping-address-select']</value>
      <webElementGuid>6b9a56cc-679b-405b-b988-dcf8edd60e31</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='shipping-addresses-form']/div/div/select</value>
      <webElementGuid>8f80e680-c51d-4892-b9f9-6ed36b0838a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select a shipping address from your address book or enter a new address.'])[1]/following::select[1]</value>
      <webElementGuid>5f954677-5747-43ee-ac24-e29821b19abe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Shipping address'])[1]/following::select[1]</value>
      <webElementGuid>5d37201c-3ccb-442c-9d17-9883e4ae38d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='First name:'])[2]/preceding::select[1]</value>
      <webElementGuid>28fabfe2-267e-4f7a-b321-8344591b1488</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[9]/preceding::select[1]</value>
      <webElementGuid>f943a129-1a9b-4b72-9e30-447a23b26abe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div/div/div/div/div/select</value>
      <webElementGuid>a6d11419-9650-400a-b039-987a2acd681b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'shipping_address_id' and @id = 'shipping-address-select' and (text() = '
                            Fazaa Abdillah, das213, asda asda, Algeria
                            Fazaa Abdillah, asdas, asd 1223, Indonesia
                        New Address
                    ' or . = '
                            Fazaa Abdillah, das213, asda asda, Algeria
                            Fazaa Abdillah, asdas, asd 1223, Indonesia
                        New Address
                    ')]</value>
      <webElementGuid>d86a86cf-cc05-45fe-b588-2677fb88fb3b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

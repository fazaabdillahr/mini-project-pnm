<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input_FirstName</name>
   <tag></tag>
   <elementGuidId>f9837fcb-9f47-4728-8165-0d24df9fec45</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#FirstName</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;FirstName&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>f44d06cc-8829-4b5a-b052-5e1b9a0f664b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-box single-line</value>
      <webElementGuid>4a99f87e-79a7-4233-9c1d-cfc21e20c50d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>fceb667c-7bff-4a8e-b741-9f385f3cdc0e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val-required</name>
      <type>Main</type>
      <value>First name is required.</value>
      <webElementGuid>11957d48-fbab-4fd2-8d74-db33331020df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>FirstName</value>
      <webElementGuid>6751204f-53bd-4b2d-bd8d-954c7938b2fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>FirstName</value>
      <webElementGuid>ea1b135b-0701-42ab-b8e9-b32a310471bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>97e23213-2117-455b-a470-30c6ad152bfb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FirstName&quot;)</value>
      <webElementGuid>b942af45-ba88-443e-8ba6-4d51a5a570fb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='FirstName']</value>
      <webElementGuid>3fcb93a3-128c-4a94-bab9-95dc6f6020d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/input</value>
      <webElementGuid>57ecc1a0-b4f7-4de6-943f-d9e7e0793c74</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'FirstName' and @name = 'FirstName' and @type = 'text']</value>
      <webElementGuid>1a389f03-032a-4101-91a1-633d96df1cb6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

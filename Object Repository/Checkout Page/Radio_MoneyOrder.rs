<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Radio_MoneyOrder</name>
   <tag></tag>
   <elementGuidId>2cda9dc8-e35b-4c59-8fdb-7c616e13a079</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#paymentmethod_2</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='paymentmethod_2']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>10a87a86-f98d-409d-9b46-4fa9be8b0452</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>paymentmethod_2</value>
      <webElementGuid>702f8e6c-0433-4413-8dd1-1564557d5a8e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>radio</value>
      <webElementGuid>c921f35d-e23d-41d5-8a13-d047d5dccfc9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>paymentmethod</value>
      <webElementGuid>1c83fd4d-9bb2-4fa2-aa64-7ba0f857119e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Payments.Manual</value>
      <webElementGuid>5ecfdcf7-0b94-4f6e-b7a1-dd8e44b008b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;paymentmethod_2&quot;)</value>
      <webElementGuid>5d97d612-290c-42cc-8e74-80faf0700f49</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='paymentmethod_2']</value>
      <webElementGuid>d5034e2b-df27-469f-a608-c0485e56b704</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-payment-method-load']/div/div/ul/li[3]/div/div[2]/input</value>
      <webElementGuid>88b4d75e-cc5b-4a6b-9c90-febca8b9aa5c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/div/div[2]/input</value>
      <webElementGuid>f237769c-076c-4a53-a84d-d4d26ed24f2a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'paymentmethod_2' and @type = 'radio' and @name = 'paymentmethod']</value>
      <webElementGuid>8c1cd881-dc48-44fe-b89a-d7b7a9c142fd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

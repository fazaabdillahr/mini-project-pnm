<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Email asdasdccgmail.com</name>
   <tag></tag>
   <elementGuidId>3ae6cf78-1a47-4831-b14e-5bf1502af0c6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.email</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/div/ul/li[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>b3e9a540-8250-4f6a-9be4-c1866fa2a279</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>5d96627e-840f-4d29-8e73-ea24f2aa524a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                Email: asdasdcc@gmail.com
            </value>
      <webElementGuid>c80aa9e9-91c6-4e07-a6d5-ab1d35af2e25</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-confirm-order-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;order-summary-body&quot;]/div[@class=&quot;order-summary-content&quot;]/div[@class=&quot;order-review-data&quot;]/ul[@class=&quot;billing-info&quot;]/li[@class=&quot;email&quot;]</value>
      <webElementGuid>efc3b6b4-3eac-4a82-ae07-2b0ead4621bb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/div/ul/li[3]</value>
      <webElementGuid>492f6828-98c3-45cf-b2b8-db3a23ac6b91</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fazaa Abdillah'])[1]/following::li[1]</value>
      <webElementGuid>afd49252-1683-4e20-ad63-324a93d06c76</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Billing Address'])[1]/following::li[2]</value>
      <webElementGuid>4daa7f2f-68b4-4443-9116-acf67edc7930</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Phone: 132123'])[1]/preceding::li[1]</value>
      <webElementGuid>00860484-388b-4b71-b945-62b59216e38a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fax: 12312'])[1]/preceding::li[2]</value>
      <webElementGuid>3e692d50-3364-4e6e-9f68-153cad2ee0b7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Email: asdasdcc@gmail.com']/parent::*</value>
      <webElementGuid>49047190-228d-4c97-b708-f098253f1a4f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/ul/li[3]</value>
      <webElementGuid>9e0d86a4-1b39-4630-b745-366f5dcacb66</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = '
                Email: asdasdcc@gmail.com
            ' or . = '
                Email: asdasdcc@gmail.com
            ')]</value>
      <webElementGuid>2f95bc96-00d7-4ab9-b608-a92a0e155299</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

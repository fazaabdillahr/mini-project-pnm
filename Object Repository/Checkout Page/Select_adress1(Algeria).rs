<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Select_adress1(Algeria)</name>
   <tag></tag>
   <elementGuidId>cb9eefa9-de24-4413-ad4a-4c0c042fcc8b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#shipping-address-select</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='shipping-address-select']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>2106e317-2aaf-48f9-bf9a-6553cdd92247</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>shipping_address_id</value>
      <webElementGuid>290c94c5-62fa-427b-a15c-0bf01b13d7b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>shipping-address-select</value>
      <webElementGuid>99a2e90d-3be3-421d-9024-0aab14fd7a38</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>address-select</value>
      <webElementGuid>aabcc703-aa26-4b93-8d1d-e649d47bcf86</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>Shipping.newAddress(!this.value)</value>
      <webElementGuid>727ca189-558e-4c1f-bb80-1c860bf45c94</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            Fazaa Abdillah, das213, asda asda, Algeria
                            Fazaa Abdillah, asdas, asd 1223, Indonesia
                        New Address
                    </value>
      <webElementGuid>b16e30b3-32b3-4b63-a186-c946e07cc88a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;shipping-address-select&quot;)</value>
      <webElementGuid>ef1d3c64-8988-4e0a-a2ab-c0210275ff88</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='shipping-address-select']</value>
      <webElementGuid>63bb5d44-bfd4-414d-a352-91184a16c6f5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='shipping-addresses-form']/div/div/select</value>
      <webElementGuid>fff7dd44-99e3-4450-bff0-ba7f4b2b0201</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select a shipping address from your address book or enter a new address.'])[1]/following::select[1]</value>
      <webElementGuid>8dc8d225-4789-4cbc-be95-24bf06e69be6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Shipping address'])[1]/following::select[1]</value>
      <webElementGuid>7a6f3592-669a-4cf0-bc5c-8fe344c383dd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='First name:'])[2]/preceding::select[1]</value>
      <webElementGuid>3224ae69-9359-48d5-8862-bc7d4dfefa82</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[9]/preceding::select[1]</value>
      <webElementGuid>905d6b03-0ba1-4ea5-ae97-7b403ef3984c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div/div/div/div/div/select</value>
      <webElementGuid>f258b503-15a4-4603-9f5e-00c907c03583</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'shipping_address_id' and @id = 'shipping-address-select' and (text() = '
                            Fazaa Abdillah, das213, asda asda, Algeria
                            Fazaa Abdillah, asdas, asd 1223, Indonesia
                        New Address
                    ' or . = '
                            Fazaa Abdillah, das213, asda asda, Algeria
                            Fazaa Abdillah, asdas, asd 1223, Indonesia
                        New Address
                    ')]</value>
      <webElementGuid>6c2d91d9-0cf5-4851-95cd-3835a5b0f334</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Order number 1630167</name>
   <tag></tag>
   <elementGuidId>9d3b0514-852d-4419-b901-22a4da6f4489</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>ul.details > li</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Your order has been successfully processed!'])[1]/following::li[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>1a4e915d-c164-4b49-9edb-fc995cb8bf64</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    Order number: 1630167
                </value>
      <webElementGuid>dfa0222d-3732-43ff-855c-50ae5cc8856e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-1&quot;]/div[@class=&quot;page checkout-page&quot;]/div[@class=&quot;page-body checkout-data&quot;]/div[@class=&quot;section order-completed&quot;]/ul[@class=&quot;details&quot;]/li[1]</value>
      <webElementGuid>e5d1485a-e02e-41ac-a5b6-d36ae656be4f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Your order has been successfully processed!'])[1]/following::li[1]</value>
      <webElementGuid>fc86ba2b-813c-4a1b-8f7d-638f9f4a27ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Thank you'])[1]/following::li[1]</value>
      <webElementGuid>bb943aae-b745-455b-871e-ad87837d9571</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Click here for order details.'])[1]/preceding::li[1]</value>
      <webElementGuid>791579b7-008f-474d-a2c3-7fea4be5cfd5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/preceding::li[2]</value>
      <webElementGuid>6430ce9d-f2de-4bdd-a322-8f28a42e29ee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Order number: 1630167']/parent::*</value>
      <webElementGuid>36071289-129b-4f3e-b7bd-6984e8691114</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[4]/div/div/div[2]/div/ul/li</value>
      <webElementGuid>f60c0dd6-6155-4078-8905-85db6d5a779e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = '
                    Order number: 1630167
                ' or . = '
                    Order number: 1630167
                ')]</value>
      <webElementGuid>84367daf-4ca3-4e42-bcfe-8f449980a054</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

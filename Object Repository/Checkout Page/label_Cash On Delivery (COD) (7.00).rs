<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_Cash On Delivery (COD) (7.00)</name>
   <tag></tag>
   <elementGuidId>f13cfe65-222b-46b7-b997-5ce29db9f498</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.payment-details > label</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-payment-method-load']/div/div/ul/li/div/div[2]/label</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>3629298d-6215-40ca-a34e-7806411bd81e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>for</name>
      <type>Main</type>
      <value>paymentmethod_0</value>
      <webElementGuid>31d281fa-66ab-4dae-97f7-a6a5223065f0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Cash On Delivery (COD) (7.00)</value>
      <webElementGuid>bf71be26-900a-4bdc-b16f-a892bdfd7a26</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-payment-method-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;section payment-method&quot;]/ul[@class=&quot;method-list&quot;]/li[1]/div[@class=&quot;method-name&quot;]/div[@class=&quot;payment-details&quot;]/label[1]</value>
      <webElementGuid>62843f27-9ce6-4a62-a722-e58e9be0c5e3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-payment-method-load']/div/div/ul/li/div/div[2]/label</value>
      <webElementGuid>8b2ed355-c368-4a32-8405-5b3b8ec289f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payment method'])[1]/following::label[2]</value>
      <webElementGuid>e246e69d-eff4-4eec-8ad5-93ea51cfb02f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading next step...'])[3]/following::label[2]</value>
      <webElementGuid>2db440fb-1b67-467f-8dfd-7e6f091ed0b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Check / Money Order (5.00)'])[1]/preceding::label[2]</value>
      <webElementGuid>a1c85332-28b5-42f0-a3e8-54e8211acf9e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Credit Card'])[1]/preceding::label[4]</value>
      <webElementGuid>31f3f0ac-ad08-4e22-abc0-6c64e34707ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Cash On Delivery (COD) (7.00)']/parent::*</value>
      <webElementGuid>9cd0a58c-0a38-493f-bd32-e82a0a7219e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/div/div[2]/label</value>
      <webElementGuid>874b2074-a05c-4204-8358-1682c55654d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//label[(text() = 'Cash On Delivery (COD) (7.00)' or . = 'Cash On Delivery (COD) (7.00)')]</value>
      <webElementGuid>f30a7951-c6d6-4747-a7ba-d822fc8a6e4d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

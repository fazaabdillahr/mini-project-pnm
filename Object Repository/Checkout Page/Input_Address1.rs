<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input_Address1</name>
   <tag></tag>
   <elementGuidId>9b3da458-697d-48f5-b591-cfb07d9db27a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#BillingNewAddress_Address1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='BillingNewAddress_Address1']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>4b86ffad-b4ca-45aa-9d7e-e406e5ddc903</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-box single-line</value>
      <webElementGuid>3e6668e3-871e-4a46-8a1a-33a9e67d1be9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>9bee0f4a-26f6-42b4-893f-dca7915d1af3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val-required</name>
      <type>Main</type>
      <value>Street address is required</value>
      <webElementGuid>668a5d9b-219c-44f6-a20a-1a12c87b8fcf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>BillingNewAddress_Address1</value>
      <webElementGuid>2f626f04-e2c1-4795-885c-2e6d6e3648dc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>BillingNewAddress.Address1</value>
      <webElementGuid>7f40edda-ac17-4c8c-94d0-ff9f4a03c871</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>5a1d3e0f-5045-48ea-b533-ba0e2be6c5c4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;BillingNewAddress_Address1&quot;)</value>
      <webElementGuid>315f04b2-9553-40a0-b025-fdcdde454ee2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='BillingNewAddress_Address1']</value>
      <webElementGuid>e22fb796-516e-4308-8e1c-a5f89751025a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='billing-new-address-form']/div/div/div/div[8]/input</value>
      <webElementGuid>18c884d4-d887-4310-a5cc-524e546839b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/input</value>
      <webElementGuid>e92d1c71-62e0-465b-9716-189ec26fd219</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'BillingNewAddress_Address1' and @name = 'BillingNewAddress.Address1' and @type = 'text']</value>
      <webElementGuid>2e3209ad-3a37-4177-9602-49d763bf3ce9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

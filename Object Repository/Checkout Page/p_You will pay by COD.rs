<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_You will pay by COD</name>
   <tag></tag>
   <elementGuidId>3818697c-9b1f-4295-9eff-cfedabf711ae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>td > p</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-payment-info-load']/div/div/div/table/tbody/tr/td/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>e7df3f10-7d99-4332-9161-8ae8a5afc090</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>You will pay by COD</value>
      <webElementGuid>87aaa6f9-eaa3-4b98-872c-63ce4411dbad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-payment-info-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;section payment-info&quot;]/div[@class=&quot;info&quot;]/table[1]/tbody[1]/tr[1]/td[1]/p[1]</value>
      <webElementGuid>9cd419c6-43db-44c5-b998-e2927d70f5b8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-payment-info-load']/div/div/div/table/tbody/tr/td/p</value>
      <webElementGuid>8c6894b1-22e3-4a48-807c-786cb89682eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payment information'])[1]/following::p[1]</value>
      <webElementGuid>e22d7f23-adf8-4f6e-8acb-c8bdc9a66d0e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading next step...'])[4]/following::p[1]</value>
      <webElementGuid>7cb91a92-adf5-41d0-8c92-34632c9bf70e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='«'])[4]/preceding::p[1]</value>
      <webElementGuid>7eee5ecc-f1db-47a1-98f2-c8fd1abcfc75</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading next step...'])[5]/preceding::p[2]</value>
      <webElementGuid>8b3b0420-4e53-4ee5-b139-349e6a703173</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='You will pay by COD']/parent::*</value>
      <webElementGuid>12de5e85-44de-4b1e-aab7-dd9543da9914</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/p</value>
      <webElementGuid>33b068e6-5d90-479e-98f6-614233af3b97</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'You will pay by COD' or . = 'You will pay by COD')]</value>
      <webElementGuid>3949bdd2-7219-41e1-89df-277449d2b9a7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

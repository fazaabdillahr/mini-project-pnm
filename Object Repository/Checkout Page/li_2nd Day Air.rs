<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_2nd Day Air</name>
   <tag></tag>
   <elementGuidId>c6d20134-317b-4052-a20e-3e341c935e6c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.shipping-method</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/div/ul[2]/li[11]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>301eceb7-9697-4d08-a7be-8166e0754226</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>shipping-method</value>
      <webElementGuid>d888b6f8-2ad3-4007-973b-9f496d8c5b6b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    2nd Day Air
                </value>
      <webElementGuid>ff5dce7e-fb22-4512-a1ba-95fd10fa1c52</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-confirm-order-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;order-summary-body&quot;]/div[@class=&quot;order-summary-content&quot;]/div[@class=&quot;order-review-data&quot;]/ul[@class=&quot;shipping-info&quot;]/li[@class=&quot;shipping-method&quot;]</value>
      <webElementGuid>7d3260e9-79ac-41b7-a05a-e425b66a3270</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/div/ul[2]/li[11]</value>
      <webElementGuid>66cbdfa4-c8e1-49db-b4c1-9642c77f63c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Shipping Method'])[1]/following::li[1]</value>
      <webElementGuid>8defbae6-3c00-46f4-9aa6-841646d290f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Indonesia'])[4]/following::li[2]</value>
      <webElementGuid>86cc6993-5524-4dea-b8ba-7973b4801bec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Product(s)'])[1]/preceding::li[1]</value>
      <webElementGuid>7cf82277-6f7f-409c-be7b-53b033a713ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Price'])[1]/preceding::li[1]</value>
      <webElementGuid>9a450fe0-38bb-4ee3-a13e-b377a1835dbc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='2nd Day Air']/parent::*</value>
      <webElementGuid>372f0de3-119f-4c79-8ecd-1aa2103f810b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//ul[2]/li[11]</value>
      <webElementGuid>b92f4335-e675-4c47-8cf6-5088fc55dc81</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = '
                    2nd Day Air
                ' or . = '
                    2nd Day Air
                ')]</value>
      <webElementGuid>b97059e9-e71a-444f-a836-726fb1b88bbc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

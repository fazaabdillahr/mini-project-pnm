<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Fazaa Abdillah</name>
   <tag></tag>
   <elementGuidId>5cd02606-f56e-4a05-87d9-7bd5f703c9ab</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.name</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/div/ul/li[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>54951cdf-6fb5-44eb-8644-a5ce1e322c72</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>name</value>
      <webElementGuid>04de7f24-83de-47f0-acc6-d3ecc72acfd5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                Fazaa Abdillah
            </value>
      <webElementGuid>f409f798-d3bf-47c2-b30e-4af83985046e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-confirm-order-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;order-summary-body&quot;]/div[@class=&quot;order-summary-content&quot;]/div[@class=&quot;order-review-data&quot;]/ul[@class=&quot;billing-info&quot;]/li[@class=&quot;name&quot;]</value>
      <webElementGuid>e8630c79-3f2b-446f-b7bd-12cc19297ecb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/div/ul/li[2]</value>
      <webElementGuid>39e368dd-18e9-45c4-8bb0-cb24eff670ce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Billing Address'])[1]/following::li[1]</value>
      <webElementGuid>f1467f2a-8f14-42bc-ab72-2ac403e92a25</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Confirm order'])[1]/following::li[2]</value>
      <webElementGuid>61e6175a-394e-4c23-b651-29ec0c4bd431</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email: asdasdcc@gmail.com'])[1]/preceding::li[1]</value>
      <webElementGuid>490471ba-a2e6-45e2-93f1-bcf5daa239e0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Phone: 132123'])[1]/preceding::li[2]</value>
      <webElementGuid>2a637626-63c2-4a53-9cef-c5627c9b46bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Fazaa Abdillah']/parent::*</value>
      <webElementGuid>50a54be9-79e6-46c2-bd5a-6fd309fdb542</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/ul/li[2]</value>
      <webElementGuid>ce98f257-a1d5-4b5a-ae49-fa9a83d4e059</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = '
                Fazaa Abdillah
            ' or . = '
                Fazaa Abdillah
            ')]</value>
      <webElementGuid>39074145-8df2-40b7-b9da-abe4607d7b85</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

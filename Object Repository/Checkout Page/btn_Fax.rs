<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_Fax</name>
   <tag></tag>
   <elementGuidId>0ac8601f-a17c-46ba-a4a6-abf6abb87754</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input.button-1.new-address-next-step-button</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@value='Continue']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>9b61d58b-c769-45f2-9034-f110b3c75b26</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>0d7386b3-994a-4219-a20b-af753c325dff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Continue</value>
      <webElementGuid>dbec2c34-f91e-4e3b-a373-1231921638e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button-1 new-address-next-step-button</value>
      <webElementGuid>e813619c-6eb8-4b3d-83b3-6edde55d8494</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>Billing.save()</value>
      <webElementGuid>5fb3c2da-4edd-4b84-a2d5-1fd27816aa7e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Continue</value>
      <webElementGuid>27a11cf1-5440-46bd-95e1-14ecca6f833a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;billing-buttons-container&quot;)/input[@class=&quot;button-1 new-address-next-step-button&quot;]</value>
      <webElementGuid>da19ea19-dedd-421b-b254-7d0834ee533e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@value='Continue']</value>
      <webElementGuid>f9571243-dd91-4931-ac1e-9c7df4da6a16</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='billing-buttons-container']/input</value>
      <webElementGuid>9f33f023-0aee-42dd-973d-44a1cb8ab0d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/input</value>
      <webElementGuid>2d144570-268b-46dc-9e92-9bd4e063d401</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'button' and @title = 'Continue']</value>
      <webElementGuid>2db32627-007f-4088-b1bc-6e87904b49ff</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

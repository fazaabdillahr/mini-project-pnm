<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Phone 132123</name>
   <tag></tag>
   <elementGuidId>4ba3ae10-ea78-4cca-b67e-d412675dcc2c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.phone</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/div/ul/li[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>eb3785c1-6a6a-407f-af23-fda7f94c5ed0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>phone</value>
      <webElementGuid>206bac44-2b4e-4760-9cc9-58e2e98e1a5e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    Phone: 132123
                </value>
      <webElementGuid>95750da0-ddd7-42a3-9f90-b3993f169477</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-confirm-order-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;order-summary-body&quot;]/div[@class=&quot;order-summary-content&quot;]/div[@class=&quot;order-review-data&quot;]/ul[@class=&quot;billing-info&quot;]/li[@class=&quot;phone&quot;]</value>
      <webElementGuid>24ebdad7-ece2-435a-b66d-59317b31300e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/div/ul/li[4]</value>
      <webElementGuid>1593f422-9483-4b9b-bece-d377008d7dd8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email: asdasdcc@gmail.com'])[1]/following::li[1]</value>
      <webElementGuid>ec8d06d0-5aba-41cc-ac68-9f9637e50472</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fazaa Abdillah'])[1]/following::li[2]</value>
      <webElementGuid>1eb2c27a-ae9b-4b2e-aeee-96e4aa7e3f1c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fax: 12312'])[1]/preceding::li[1]</value>
      <webElementGuid>57de0e1b-63f5-4986-9f1a-4dc7bf177621</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='asdas'])[1]/preceding::li[2]</value>
      <webElementGuid>b8480a70-56be-4d04-82ae-ed173729cd30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Phone: 132123']/parent::*</value>
      <webElementGuid>5fbbc5ed-49cf-4808-ad8b-2bc5b560c4b0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/ul/li[4]</value>
      <webElementGuid>8c670f52-9a20-4599-9471-e18fd5ad2a88</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = '
                    Phone: 132123
                ' or . = '
                    Phone: 132123
                ')]</value>
      <webElementGuid>48d01cdd-a0c4-4f02-9bf0-200e5b03c79e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

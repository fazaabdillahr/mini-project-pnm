<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Cash On Delivery (COD)</name>
   <tag></tag>
   <elementGuidId>fc44cdcf-3c1b-4788-881e-d96bb552eea2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.payment-method</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/div/ul/li[11]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>9720bd3e-11a3-47ad-8167-51926144b77e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>payment-method</value>
      <webElementGuid>b5476724-9723-4cdf-858b-e0c9baff4846</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        Cash On Delivery (COD)
                </value>
      <webElementGuid>ff8c2ce1-52ff-474a-abae-65f9cabc5ba4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-confirm-order-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;order-summary-body&quot;]/div[@class=&quot;order-summary-content&quot;]/div[@class=&quot;order-review-data&quot;]/ul[@class=&quot;billing-info&quot;]/li[@class=&quot;payment-method&quot;]</value>
      <webElementGuid>685368d7-dcf1-4ea4-b64c-6e095c7bd44e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/div/ul/li[11]</value>
      <webElementGuid>54c74cdc-c139-449c-babc-5ea0ec83cacd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payment Method'])[1]/following::li[1]</value>
      <webElementGuid>b5dfa46f-c1bd-4ea2-891c-35cead85cb89</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Indonesia'])[3]/following::li[2]</value>
      <webElementGuid>15927ce1-d63c-4a4e-8fdb-05e6a10c8bae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Shipping Address'])[1]/preceding::li[1]</value>
      <webElementGuid>e49a4193-e378-4ed3-be7c-85c03741f01c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fazaa Abdillah'])[2]/preceding::li[2]</value>
      <webElementGuid>ebeab70f-0ed5-4c6d-a843-f76aa5a59569</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Cash On Delivery (COD)']/parent::*</value>
      <webElementGuid>4ea3a699-b194-4f15-943c-d736f2d8bbea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[11]</value>
      <webElementGuid>2e1f47b1-95f8-4ae9-ac4d-0eca6c01c3c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = '
                        Cash On Delivery (COD)
                ' or . = '
                        Cash On Delivery (COD)
                ')]</value>
      <webElementGuid>60faf6de-2f43-4e84-b307-595a0d599abd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_inputcart</name>
   <tag></tag>
   <elementGuidId>88ccdab7-e8a9-4139-84d2-a72859ce44a9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[4]/div[1]/div[4]/div[3]/div/div/div[3]/div[4]/div/div[2]/div[3]/div[2]/input</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#add-to-cart-button-72</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>f3e6324f-3f98-4175-8761-301670c564f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>caa61620-5ac2-4f9c-b769-fec9cf4e1b65</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>add-to-cart-button-72</value>
      <webElementGuid>b78bd027-4bf9-486e-a65d-14ef8e0adece</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button-1 add-to-cart-button</value>
      <webElementGuid>e7c21c4e-612f-46a5-b823-e152469ed5ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Add to cart</value>
      <webElementGuid>9e8f6f53-4a75-4ab4-89c6-0c544b723970</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-productid</name>
      <type>Main</type>
      <value>72</value>
      <webElementGuid>0bcf35a8-ec5f-40e1-98c4-bf62d7a00d3f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>AjaxCart.addproducttocart_details('/addproducttocart/details/72/1', '#product-details-form');return false;</value>
      <webElementGuid>d6d908b0-e916-4199-a2a7-e2ac2f76a259</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;add-to-cart-button-72&quot;)</value>
      <webElementGuid>08554ede-e886-425b-aa8d-cb61c1fa6268</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='add-to-cart-button-72']</value>
      <webElementGuid>bade2628-e63b-470b-b13f-dd7b286de0c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='product-details-form']/div/div/div[2]/div[8]/div/input[2]</value>
      <webElementGuid>3ec9a6f3-96db-4a57-8120-3020d7878975</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/input[2]</value>
      <webElementGuid>aa7fb84d-e385-4688-8091-42dbaa62a0a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'button' and @id = 'add-to-cart-button-72']</value>
      <webElementGuid>5e40a918-f7de-47cc-b0d7-4fab51e03905</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

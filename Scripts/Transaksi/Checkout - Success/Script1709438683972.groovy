import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Login/Login - Success'), [('Email') : 'asdasdcc@gmail.com', ('Password') : 'Abdillah123'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Checkout Page/btn_addchart'))

WebUI.click(findTestObject('Object Repository/Page_Demo Web Shop. Build your own cheap computer/input_Qty_add-to-cart-button-72'))

WebUI.click(findTestObject('Object Repository/Page_Demo Web Shop. Build your own cheap computer/a_Shopping cart                    (1)'))

WebUI.click(findTestObject('Object Repository/Page_Demo Web Shop. Shopping Cart/input_I agree with the terms of service and I adhere to them unconditionally_termsofservice'))

WebUI.click(findTestObject('Object Repository/Page_Demo Web Shop. Shopping Cart/button_Checkout'))

WebUI.selectOptionByValue(findTestObject('Checkout Page/Select_Pertama'), '3504264', true)

WebUI.click(findTestObject('Checkout Page/btn_nextShip'))

WebUI.selectOptionByValue(findTestObject('Checkout Page/Select_Pertama'), '3504264', true)

WebUI.click(findTestObject('Checkout Page/btn_ConShipMethod'))

WebUI.click(findTestObject('Checkout Page/Radio_ShippingGround'))

SHIPAIR = WebUI.getText(findTestObject('Checkout Page/P_ShippingAIR'))

WebUI.click(findTestObject('Checkout Page/btn_next-payment-method'))

WebUI.click(findTestObject('Checkout Page/Radio_COD'))

CODSATU = WebUI.getText(findTestObject('Checkout Page/Radio_COD'))

WebUI.click(findTestObject('Checkout Page/btn_next-payment-info'))

WebUI.verifyElementText(findTestObject('Checkout Page/P_COD'), 'You will pay by COD')

WebUI.click(findTestObject('Checkout Page/btn_next-confirm-order'))

WebUI.getText(findTestObject('Checkout Page/li_Fazaa Abdillah'))

CODDUA = WebUI.getText(findTestObject('Checkout Page/P_COD'))

def CODSATU = CODDUA

AIRSHIP = WebUI.getText(findTestObject('Checkout Page/P_ShippingAIR'))

def SHIPAIR = AIRSHIP

WebUI.click(findTestObject('Checkout Page/btn_confiirmcheckout'))

WebUI.verifyElementText(findTestObject('Checkout Page/CheckoutStatus'), 'Your order has been successfully processed!')

WebUI.click(findTestObject('Checkout Page/btn_lastcon'))

WebUI.closeBrowser()


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demowebshop.tricentis.com/')

WebUI.click(findTestObject('Register Page/btn_register1'))

WebUI.click(findTestObject('Page_Demo Web Shop. Register/input_Gender_Gender'))

WebUI.setText(findTestObject('Register Page/Input_FirstName'), 'Fazaa')

WebUI.setText(findTestObject('Register Page/Input_LastName'), 'Abdillah')

WebUI.setText(findTestObject('Register Page/Input_Email'), 'fazaabdillahminiprojek@gmail.com')

user = WebUI.getText(findTestObject('Register Page/Input_Email'), FailureHandling.STOP_ON_FAILURE)

WebUI.setEncryptedText(findTestObject('Register Page/Input_Password'), 'P9kzra3rLm/Bj5/bREhCqA==')

WebUI.setEncryptedText(findTestObject('Register Page/Input_ConfirmPassword'), 'P9kzra3rLm/Bj5/bREhCqA==')

WebUI.click(findTestObject('Register Page/btn_Register'))

confirm = WebUI.getText(findTestObject('Register Page/Emailuser'))

def user = confirm

WebUI.click(findTestObject('Register Page/btn_RegisterConfirm'))

WebUI.closeBrowser()

